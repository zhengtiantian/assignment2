import libsvm.*;

import java.io.*;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.Vector;

public class RunRunRun {

    private static svm_parameter param;
    private static svm_problem prob;
    private static svm_model model;
    private static String input_train_file_name = "src/train-io.txt";
    private static String input_test_file_name = "src/test-in.txt";
    private static String model_file_name = "src/model.txt";
    private static String output_file_name = "src/test-out.txt";
    private static int predict_classification = 0;

    private static void setParam() {
        svm.svm_set_print_string_function(null);
        param = new svm_parameter();
        param.svm_type = svm_parameter.C_SVC;
        param.kernel_type = svm_parameter.RBF;
        param.degree = 3;
        param.gamma = 0;
        param.coef0 = 0;
        param.nu = 0.5;
        param.cache_size = 2048;
        param.C = 1;
        param.eps = 1e-3;
        param.p = 0.1;
        param.shrinking = 1;
        param.probability = 0;
        param.nr_weight = 0;
        param.weight_label = new int[0];
        param.weight = new double[0];

    }

    public static void main(String[] args) throws IOException {
        try {
            long b = System.currentTimeMillis();
            BufferedReader fp = new BufferedReader(new FileReader(input_train_file_name));
            setParam();
            read_problem(fp);
            model = svm.svm_train(prob, param);
            svm.svm_save_model(model_file_name, model);
            System.out.println((System.currentTimeMillis() - b));
            predict();
        } catch (FileNotFoundException e) {
            predict();
        }
    }

    private static void read_problem(BufferedReader fp) throws IOException {

        Vector<Double> vy = new Vector<Double>();
        Vector<svm_node[]> vx = new Vector<svm_node[]>();
        int max_index = 0;

        while (true) {
            String line = fp.readLine();
            if (line == null) break;
            double[] s = Arrays.stream(line.split(" ")).mapToDouble(x -> Double.parseDouble(x)).toArray();
            int len = s.length - 1;
            vy.addElement(s[12]);
            svm_node[] x = new svm_node[len];
            for (int j = 0; j < len; j++) {
                x[j] = new svm_node();
                x[j].index = (j + 1);
                x[j].value = Double.valueOf(s[j]);
            }
            if (len > 0) {
                max_index = Math.max(max_index, x[len - 1].index);
            }
            vx.addElement(x);
        }

        prob = new svm_problem();
        prob.l = vy.size();
        prob.x = new svm_node[prob.l][];
        for (int i = 0; i < prob.l; i++) {
            prob.x[i] = vx.elementAt(i);
        }
        prob.y = new double[prob.l];
        for (int i = 0; i < prob.l; i++) {
            prob.y[i] = vy.elementAt(i);
        }

        if (param.gamma == 0 && max_index > 0) {
            param.gamma = 1.0 / max_index;
        }

        if (param.kernel_type == svm_parameter.PRECOMPUTED)
            for (int i = 0; i < prob.l; i++) {
                if (prob.x[i][0].index != 0) {
                    System.err.print("Wrong kernel matrix: first column must be 0:sample_serial_number\n");
                    System.exit(1);
                }
                if ((int) prob.x[i][0].value <= 0 || (int) prob.x[i][0].value > max_index) {
                    System.err.print("Wrong input format: sample_serial_number out of range\n");
                    System.exit(1);
                }
            }

        fp.close();
    }

    private static void predict() throws IOException {
        int correct = 0;
        int total = 0;

        BufferedReader input = new BufferedReader(new FileReader(input_test_file_name));
        DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(output_file_name)));
        svm_model model = svm.svm_load_model(model_file_name);

        while (true) {
            String line = input.readLine();
            if (line == null) break;


            double target_label = predict_classification;
            double[] s = Arrays.stream(line.split(" ")).mapToDouble(x -> Double.parseDouble(x)).toArray();
            int len = s.length;
            svm_node[] x = new svm_node[len];
            for (int j = 0; j < len; j++) {
                x[j] = new svm_node();
                x[j].index = j + 1;
                x[j].value = Double.valueOf(s[j]);
            }

            double predict_label;
            predict_label = svm.svm_predict(model, x);
            output.writeBytes((int) predict_label + "\n");

            if (predict_label == target_label) {
                ++correct;
            }
            ++total;
        }


        System.out.println("Accuracy = " + (double) correct / total * 100 +
                "% (" + correct + "/" + total + ") (classification)\n");

        input.close();
        output.close();
    }

    private static void predict_probability() throws IOException {
        BufferedReader input = new BufferedReader(new FileReader(input_test_file_name));
        DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(output_file_name)));
        svm_model model = svm.svm_load_model(model_file_name);

        int correct = 0;
        int total = 0;
        double error = 0;
        double sump = 0, sumt = 0, sumpp = 0, sumtt = 0, sumpt = 0;

        int svm_type = svm.svm_get_svm_type(model);
        int nr_class = svm.svm_get_nr_class(model);
        double[] prob_estimates = null;

        if (svm_type == svm_parameter.EPSILON_SVR ||
                svm_type == svm_parameter.NU_SVR) {
        } else {
            int[] labels = new int[nr_class];
            svm.svm_get_labels(model, labels);
            prob_estimates = new double[nr_class];
            output.writeBytes("labels");
            for (int j = 0; j < nr_class; j++) {
                output.writeBytes(" " + labels[j]);
            }
            output.writeBytes("\n");
        }
        while (true) {
            String line = input.readLine();
            if (line == null) break;

            StringTokenizer st = new StringTokenizer(line, " \t\n\r\f:");

            double target_label = predict_classification;
            double[] s = Arrays.stream(line.split(" ")).mapToDouble(x -> Double.parseDouble(x)).toArray();
            int len = s.length;
            svm_node[] x = new svm_node[len];
            for (int j = 0; j < len; j++) {
                x[j] = new svm_node();
                x[j].index = j + 1;
                x[j].value = Double.valueOf(s[j]);
            }

            double predict_label;
            if (svm_type == svm_parameter.C_SVC || svm_type == svm_parameter.NU_SVC) {
                predict_label = svm.svm_predict_probability(model, x, prob_estimates);
                output.writeBytes(predict_label + " ");
                for (int j = 0; j < nr_class; j++)
                    output.writeBytes(prob_estimates[j] + " ");
                output.writeBytes("\n");
            } else {
                predict_label = svm.svm_predict(model, x);
                output.writeBytes(predict_label + "\n");
            }

            if (predict_label == target_label)
                ++correct;
            error += (predict_label - target_label) * (predict_label - target_label);
            sump += predict_label;
            sumt += target_label;
            sumpp += predict_label * predict_label;
            sumtt += target_label * target_label;
            sumpt += predict_label * target_label;
            ++total;
        }
        if (svm_type == svm_parameter.EPSILON_SVR ||
                svm_type == svm_parameter.NU_SVR) {
            System.out.println("Mean squared error = " + error / total + " (regression)\n");
            System.out.println("Squared correlation coefficient = " +
                    ((total * sumpt - sump * sumt) * (total * sumpt - sump * sumt)) /
                            ((total * sumpp - sump * sump) * (total * sumtt - sumt * sumt)) +
                    " (regression)\n");
        } else {
            System.out.println("Accuracy = " + (double) correct / total * 100 +
                    "% (" + correct + "/" + total + ") (classification)\n");
        }

        input.close();
        output.close();
    }

}




