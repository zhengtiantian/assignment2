 1. Make sure your computer has installed JDK1.8 and has configured environment variables
 2. I hope you can use jetbrain idea to run this project, and the version is 2020.3
 3. Import the project with idea, the address of the code is in README.txt
 4. If you want to regenerate the model, please import train-io.txt to the "src" directory, the format is given by you. If you just want to regenerate the results, please do not import train-io.txt, only test-in.txt, the format is also given by you.
 5. run the main function in RunRunRun.java

